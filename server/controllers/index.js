const orderBy = require('lodash.orderby');
const MAX_HOURS = 8;

const allocateAndReport = (req, res, next) => {
    const requestData = orderBy(req.body || [], 'clientId');
    let buttlers = [];

    requestData.map((data) => {
        if (data.hours > 0 && data.hours > MAX_HOURS) {
            throw new Error('Maximum of 8 Hours Allowed');
        } else {
            if (buttlers.length > 0) {
                let isButtlerFound = buttlers.find((buttler) => buttler.assigendHour + data.hours <= MAX_HOURS);
                if (isButtlerFound) {
                    isButtlerFound.requests.push(data.requestId);
                    isButtlerFound.assigendHour += data.hours;
                } else {
                    buttlers.push({ requests: [data.requestId], assigendHour: data.hours });
                }
            } else {
                buttlers.push({ requests: [data.requestId], assigendHour: data.hours });
            }
        }
    });
    res.json(buttlers);
    console.log("TCL: allocateAndReport -> buttlers", buttlers)
    // res.json(buttlers.map(b => { return { "requests": b.requests } }));
}


module.exports = {
    allocateAndReport: allocateAndReport
}
