# Server Setup

switch to the server directory ```./server```

install node modules with  ```npm install```

start the server with ```npm start```

## Server will start on ```3000``` port

# Client Setup

switch to the client directory ```./client```

install node modules with  ```npm install```

start the client server with ```npm start```

## client will start on ```4000```

# 
# Enjoy!!!