import React from 'react';
import { Card, Form, Col, Button, Alert } from 'react-bootstrap';
import './App.css';
import * as DataService from './services/data.service';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      workDetail: [{
        clientId: parseInt(Math.random() * 10),
        requestId: Math.random().toString(36).slice(-5),
        hours: Math.floor(Math.random() * 8) + 1
      }],
      responseData: [],
      error: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddNewRequest = this.handleAddNewRequest.bind(this);
    this.handleResetData = this.handleResetData.bind(this);
  }

  handleInputChange(event, index) {
    let { workDetail } = {
      ...this.state
    };

    switch (event.target.name) {
      case "clientId":
        workDetail[index][event.target.name] = parseInt(event.target.value);
        break;
      case "requestId":
        workDetail[index]['requestId'] = event.target.value;
        break;
      case "hours":
        workDetail[index][event.target.name] = parseInt(event.target.value);
        break;
      default:
        break;
    }

    this.setState({ workDetail });
  }

  handleSubmit(event) {
    event.preventDefault();

    DataService.addNewRequest(this.state.workDetail).then(res => {
      this.setState({
        responseData: res.data
      })
    }).catch(error => {
      this.setState({
        error: JSON.stringify(error)
      })
    })
  }

  handleAddNewRequest(event) {
    event.preventDefault();
    let { workDetail } = {
      ...this.state
    };

    workDetail.push({
      clientId: parseInt(Math.random() * 10),
      requestId: Math.random().toString(36).slice(-5),
      hours: Math.floor(Math.random() * 8) + 1
    })

    this.setState({ workDetail })
  }

  handleResetData(event) {
    event.preventDefault();
    let { workDetail, responseData } = {
      ...this.state
    };

    workDetail = [];
    responseData = [];

    workDetail.push({
      clientId: parseInt(Math.random() * 10),
      requestId: Math.random().toString(36).slice(-5),
      hours: Math.floor(Math.random() * 8) + 1
    })

    this.setState({ workDetail, responseData })
  }

  render() {
    return (
      <div className="container mt-5">
        <Card>
          <Card.Body>
            <div className="row">
              <div className="col-12">
                <Form onSubmit={this.handleSubmit}>
                  {
                    this.state.workDetail.map((work, index) => {
                      return (<Form.Row key={index}>
                        <Form.Group as={Col} controlId="work.clientId">
                          <Form.Label>Client Id</Form.Label>
                          <Form.Control type="number" name="clientId" value={work.clientId} placeholder="Client Id" onChange={(event) => this.handleInputChange(event, index)} />
                        </Form.Group>
                        <Form.Group as={Col} controlId="requestId">
                          <Form.Label>Request Id</Form.Label>
                          <Form.Control type="text" name="requestId" value={work.requestId} placeholder="Request Id" onChange={(event) => this.handleInputChange(event, index)} />
                        </Form.Group>
                        <Form.Group as={Col} controlId="hours">
                          <Form.Label>Hours</Form.Label>
                          <Form.Control type="number" name="hours" min="0" max="8" placeholder="0" value={work.hours} onChange={(event) => this.handleInputChange(event, index)} />
                        </Form.Group>
                        <Form.Group as={Col} className="mt-3">
                        </Form.Group>
                      </Form.Row>)

                    })
                  }
                  <Button className="mt-3 mr-2" variant="primary" type="button" onClick={this.handleAddNewRequest}>
                    Add New
                  </Button>
                  <Button className="mt-3 mr-2" variant="primary" type="button" onClick={this.handleResetData}>
                    Reset
                  </Button>
                  <Button className="mt-3" variant="primary" type="submit">
                    Submit
                  </Button>
                </Form>
              </div>

            </div>
            <div className="row mt-4">
              <div className="col-12">
                <Form.Group as="object" controlId="requestId">
                  {this.state.responseData.map((list, index) => (
                    <div key={index}>
                      <pre>{
                        JSON.stringify(list, null, 2)
                      }</pre>
                    </div>
                  ))}
                </Form.Group>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-12">
                {this.state.error &&
                  <Alert variant="danger">
                    {this.state.error}
                  </Alert>
                }
              </div>
            </div>
          </Card.Body>
        </Card>
      </div >
    );
  }
}

export default App;
