import axios from 'axios';
import { env } from '../env';

export function addNewRequest (requestData) {
    return axios.post(`${env.baseURL}`, requestData);
}
